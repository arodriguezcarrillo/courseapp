﻿using System;
using Xamarin.Forms;
using CourseApp.Controls;
using Xamarin.Forms.Platform.Android;
using CourseApp.Droid;

[assembly:ExportRenderer(typeof(ListViewClear), typeof(ListViewClear_Android))]

namespace CourseApp.Droid
{
	public class ListViewClear_Android : ListViewRenderer
	{
		public ListViewClear_Android ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			var listView = Control as global::Android.Widget.ListView;
			listView.DividerHeight = 0;
		}
	}
}

