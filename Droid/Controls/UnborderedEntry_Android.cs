﻿using System;
using CourseApp.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using CourseApp.Droid;

[assembly: ExportRenderer (typeof (UnborderedEntry), typeof (UnborderedEntry_Android))]

namespace CourseApp.Droid
{
	public class UnborderedEntry_Android : EntryRenderer
	{
		public UnborderedEntry_Android ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);
		}
	}
}

