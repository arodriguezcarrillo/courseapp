﻿using System;
using Xamarin.Forms.Platform.Android;
using Android.App;
using Xamarin.Forms;
using CourseApp.Droid;

[assembly:ExportRenderer(typeof(NavigationPage), typeof(Navigation_Android))]

namespace CourseApp.Droid
{
	public class Navigation_Android : NavigationRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.NavigationPage> e)
		{
			base.OnElementChanged (e);

			var actionBar = ((Activity)Context).ActionBar;
			actionBar.SetIcon (Android.Graphics.Color.Transparent);
		}
	}
}

