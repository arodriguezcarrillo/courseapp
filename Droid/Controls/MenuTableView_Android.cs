﻿using System;
using CourseApp;
using CourseApp.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using CourseApp.Droid;

[assembly:ExportRenderer(typeof(MenuTableView), typeof(MenuTableView_Android))]

namespace CourseApp.Droid
{
	public class MenuTableView_Android : TableViewRenderer
	{
		public MenuTableView_Android ()
		{

		}

		protected override void OnElementChanged (ElementChangedEventArgs<TableView> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			var tableview = Control as Android.Widget.ListView;
			tableview.DividerHeight = 0;
		}
	}
}

