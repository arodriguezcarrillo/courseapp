﻿using System;
using Xamarin.Forms;
using CourseApp.Controls;
using Xamarin.Forms.Platform.Android;
using CourseApp.Droid;
using Android.Graphics;

[assembly:ExportRenderer(typeof(CircleImage), typeof(CircleImage_Android))]
namespace CourseApp.Droid
{
	public class CircleImage_Android : ImageRenderer
	{
		public CircleImage_Android ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			//Control.Background = Context.GetDrawable(CourseApp.Droid.Resource.Drawable.rounded_corners);
			//Control.SetImageBitmap (roundCornerImage (BitmapFactory.DecodeResource(Context.Resources, CourseApp.Droid.Resource.Drawable.dog), 50));
		}

		protected override bool DrawChild (Canvas canvas, Android.Views.View child, long drawingTime)
		{
			try
			{
				var radius = Math.Min(Width, Height) / 2;
				var strokeWidth = 0;
				radius -= strokeWidth / 2;
				//Create path to clip
				var path = new Path();
				path.AddCircle(Width / 2, Height / 2, radius, Path.Direction.Ccw);
				canvas.Save();
				canvas.ClipPath(path);
				var result = base.DrawChild(canvas, child, drawingTime);
				canvas.Restore();
				// Create path for circle border
				path = new Path();
				path.AddCircle(Width / 2, Height / 2, radius, Path.Direction.Ccw);
				path.Dispose();
				return result;
			}
			catch (Exception ex)
			{
			}

			return base.DrawChild(canvas, child, drawingTime);
		}

	}
}