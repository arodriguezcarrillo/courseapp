﻿using System;
using CourseApp.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using CourseApp.Droid;

[assembly:ExportRenderer(typeof(MaterialProfileCell), typeof(MaterialProfileCell_Android))]
namespace CourseApp.Droid
{
	public class MaterialProfileCell_Android : ViewCellRenderer
	{

		protected override Android.Views.View GetCellCore (Cell item, Android.Views.View convertView, Android.Views.ViewGroup parent, Android.Content.Context context)
		{
			var resultView = base.GetCellCore (item, convertView, parent, context);

			resultView.SetBackgroundColor (Android.Graphics.Color.Transparent);

			return resultView;
		}
	}
}