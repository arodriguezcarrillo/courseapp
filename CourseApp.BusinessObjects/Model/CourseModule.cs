﻿using System;
using System.Collections.Generic;

namespace CourseApp.BusinessObjects
{
	public class CourseModule
	{
		public string Name {
			get;
			set;
		}

		public IList<CourseFile> Files {
			get;
			set;
		}
		
		public CourseModule ()
		{
		}
	}
}

