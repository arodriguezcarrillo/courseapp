﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace CourseApp.BusinessObjects
{
	public class User : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private string _name;
		public string Name {
			get{ return this._name; }
			set{
				this._name = value;
				this.FirePropertyChanged ("Name");
				this.FirePropertyChanged ("CompleteName");
			}
		}

		private string _surname;
		public string Surname {
			get{ return this._surname; }
			set{
				this._surname = value;
				this.FirePropertyChanged ("Surname");
				this.FirePropertyChanged ("CompleteName");
			}
		}
			
		public string CompleteName {
			get{ return Name + " " + Surname; }
		}

		private string _email;
		public string Email {
			get{ return this._email; }
			set{
				this._email = value;
				this.FirePropertyChanged ("Email");
			}
		}

		public ImageSource Image {
			get{
				return ImageSource.FromFile ("dog");
			}
		}

		private void FirePropertyChanged(string propertyName){
			if (this.PropertyChanged != null)
				this.PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
		}
	}
}

