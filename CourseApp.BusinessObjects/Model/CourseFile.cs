﻿using System;

namespace CourseApp.BusinessObjects
{
	public class CourseFile
	{
		public string Name {
			get;
			set;
		}

		public string Remote {
			get;
			set;
		}
	}
}

