﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace CourseApp.BusinessObjects
{
	public class Course
	{
		public int Id {
			get;
			set;
		}

		public string Name {
			get;
			set;
		}

		public string Excerpt {
			get;
			set;
		}

		public string Details {
			get;
			set;
		}

		public string Image {
			get;
			set;
		}

		public ImageSource ImageSource {
			get{ 
				return ImageSource.FromFile (this.Image);
			}
		}

		public IList<CourseModule> Modules {
			get;
			set;
		}

		public int ModuleNumber {
			get{ 
				return Modules != null ? Modules.Count : 0;
			}
		}

		public Course ()
		{
		}
	}
}

