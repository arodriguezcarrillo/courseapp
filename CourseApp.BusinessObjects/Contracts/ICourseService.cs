﻿using System;
using System.Collections.Generic;

namespace CourseApp.BusinessObjects
{
	public interface ICourseService
	{
		IEnumerable<Course> Courses();
	}
}

