﻿using System;

namespace CourseApp.BusinessObjects
{
	public interface IUserService
	{
		bool Register(string name, string surname, string password, string email);
		bool Login(string email, string password);
		bool IsAuthenticated();
		User GetCurrentUser();
	}
}

