﻿using System;

namespace CourseApp.BusinessObjects
{
	public class UserServiceMockup : IUserService
	{

		static User _user;

		public UserServiceMockup ()
		{
		}

		#region IUserService implementation

		public bool Register (string name, string surname, string password, string email)
		{
			_user = new User () {
				Email = email,
				Name = name,
				Surname = surname
			};
			return true;
		}

		public bool Login (string email, string password)
		{
			_user = new User () {
				Email = email,
				Name = "Alberto",
				Surname = "Rodríguez"
			};

			return true;
		}

		public bool IsAuthenticated(){
			return _user != null;
		}

		public User GetCurrentUser(){
			return _user;
		}

		#endregion
	}
}

