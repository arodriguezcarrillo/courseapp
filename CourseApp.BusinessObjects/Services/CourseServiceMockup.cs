﻿using System;
using System.Collections.Generic;

namespace CourseApp.BusinessObjects
{
	public class CourseServiceMockup : ICourseService
	{
		static IEnumerable<Course> _data;
		public CourseServiceMockup ()
		{
		}

		private void InitializeCourses(){
			_data = new List<Course> () {
				new Course(){
					Id = 1,
					Name = "Lorem ipsum",
					Image = "r128_2.jpg",
					Excerpt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sit ",
					Details = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec interdum ligula non scelerisque ullamcorper. Vivamus commodo porttitor elit, quis fermentum ipsum dictum at. Aenean quis massa nec nulla dignissim rutrum quis at ante. Maecenas lectus sapien, blandit et auctor ac, interdum vel orci. Maecenas felis leo, consequat ac orci nec, imperdiet sodales dolor. Duis consequat libero id justo malesuada, at hendrerit dolor fringilla. Ut feugiat nisl a faucibus mollis. Pellentesque vel auctor ipsum, nec tristique felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
				},
				new Course(){
					Id = 2,
					Name = "Lorem ipsum",
					Image = "r128_2.jpg",
					Excerpt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sit ",
					Details = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec interdum ligula non scelerisque ullamcorper. Vivamus commodo porttitor elit, quis fermentum ipsum dictum at. Aenean quis massa nec nulla dignissim rutrum quis at ante. Maecenas lectus sapien, blandit et auctor ac, interdum vel orci. Maecenas felis leo, consequat ac orci nec, imperdiet sodales dolor. Duis consequat libero id justo malesuada, at hendrerit dolor fringilla. Ut feugiat nisl a faucibus mollis. Pellentesque vel auctor ipsum, nec tristique felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
				},
				new Course(){
					Id = 3,
					Name = "Lorem ipsum",
					Image = "r128_2.jpg",
					Excerpt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sit ",
					Details = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec interdum ligula non scelerisque ullamcorper. Vivamus commodo porttitor elit, quis fermentum ipsum dictum at. Aenean quis massa nec nulla dignissim rutrum quis at ante. Maecenas lectus sapien, blandit et auctor ac, interdum vel orci. Maecenas felis leo, consequat ac orci nec, imperdiet sodales dolor. Duis consequat libero id justo malesuada, at hendrerit dolor fringilla. Ut feugiat nisl a faucibus mollis. Pellentesque vel auctor ipsum, nec tristique felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
				},
				new Course(){
					Id = 4,
					Name = "Lorem ipsum",
					Image = "r128_2.jpg",
					Excerpt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sit ",
					Details = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec interdum ligula non scelerisque ullamcorper. Vivamus commodo porttitor elit, quis fermentum ipsum dictum at. Aenean quis massa nec nulla dignissim rutrum quis at ante. Maecenas lectus sapien, blandit et auctor ac, interdum vel orci. Maecenas felis leo, consequat ac orci nec, imperdiet sodales dolor. Duis consequat libero id justo malesuada, at hendrerit dolor fringilla. Ut feugiat nisl a faucibus mollis. Pellentesque vel auctor ipsum, nec tristique felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
				}
			};
		}

		#region ICourseService implementation

		public System.Collections.Generic.IEnumerable<Course> Courses ()
		{
			if (_data == null)
				this.InitializeCourses ();

			return _data;
		}

		#endregion
	}
}

