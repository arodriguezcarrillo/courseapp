﻿using System;
using Xamarin.Forms;
using CourseApp.Controls;
using CourseApp.iOS;
using Xamarin.Forms.Platform.iOS;
using CoreGraphics;

[assembly:ExportRenderer(typeof(CircleImage), typeof(CircleImage_iOS))]
namespace CourseApp.iOS
{
	public class CircleImage_iOS : ImageRenderer
	{
		public CircleImage_iOS ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;


			CGSize size = Control.Layer.PreferredFrameSize();
			double min = Math.Min(size.Width, size.Height);
			Control.Layer.CornerRadius = 32;
			Control.ClipsToBounds = true;
		}
	}
}