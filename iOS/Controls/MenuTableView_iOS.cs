﻿using System;
using CourseApp;
using CourseApp.Controls;
using CourseApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly:ExportRenderer(typeof(MenuTableView), typeof(MenuTableView_iOS))]

namespace CourseApp.iOS
{
	public class MenuTableView_iOS : TableViewRenderer
	{
		public MenuTableView_iOS ()
		{

		}

		protected override void OnElementChanged (ElementChangedEventArgs<TableView> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			var tableView = Control as UITableView;
			tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
		}
	}
}

