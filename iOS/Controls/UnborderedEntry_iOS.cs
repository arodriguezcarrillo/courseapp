﻿using System;
using CoreAnimation;
using CoreGraphics;
using CourseApp.Controls;
using CourseApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (UnborderedEntry), typeof (UnborderedEntry_iOS))]

namespace CourseApp.iOS
{
	public class UnborderedEntry_iOS : EntryRenderer
	{
		public UnborderedEntry_iOS ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);

			Control.BorderStyle = UITextBorderStyle.None;
		}
	}
}

