﻿using System;
using Xamarin.Forms.Platform.iOS;
using CourseApp.iOS;
using Xamarin.Forms;
using UIKit;
using CourseApp.Controls;

[assembly:ExportRenderer(typeof(ListViewClear), typeof(ListViewClear_iOS))]

namespace CourseApp.iOS
{
	public class ListViewClear_iOS : ListViewRenderer
	{
		public ListViewClear_iOS ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			var tableView = Control as UITableView;
			tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
		}
	}
}

