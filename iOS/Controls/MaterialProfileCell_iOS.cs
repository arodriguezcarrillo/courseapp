﻿using System;
using CourseApp.Controls;
using CourseApp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;

[assembly:ExportRenderer(typeof(MaterialProfileCell), typeof(MaterialProfileCell_iOS))]
namespace CourseApp.iOS
{
	public class MaterialProfileCell_iOS : ViewCellRenderer
	{
		public override UIKit.UITableViewCell GetCell (Cell item, UIKit.UITableViewCell reusableCell, UIKit.UITableView tv)
		{
			var result = base.GetCell (item, reusableCell, tv);

			result.SelectedBackgroundView = new UIKit.UIView {
				BackgroundColor = UIColor.Clear
			};

			return result;
		}
	}
}