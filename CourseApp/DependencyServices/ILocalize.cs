﻿using System;
using System.Globalization;

namespace CourseApp
{
	public interface ILocalize
	{
		CultureInfo GetCurrentCultureInfo ();
	}
}
