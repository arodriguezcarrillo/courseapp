﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace CourseApp.Controls
{
	public partial class TopBarMenu : ContentView
	{

		public IList<View> Children {
			get{
				return this.slData.Children;
			}
			set{
				this.slData.Children.Clear ();
				foreach (var item in value) {
					this.slData.Children.Add (item);
				}
			}
		}

		void ButtonEvent (object sender, EventArgs e)
		{
			
		}

		public TopBarMenu ()
		{
			InitializeComponent ();

		}
	}
}

