﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace CourseApp.Controls
{
	public partial class MaterialProfileCell : ViewCell
	{
		public static readonly BindableProperty TitleProperty = 
			BindableProperty.Create<MaterialProfileCell, string>(ctrl => ctrl.Title,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (MaterialProfileCell)bindable;
					ctrl.Title = newValue;
				});

		public static readonly BindableProperty SubtitleProperty = 
			BindableProperty.Create<MaterialProfileCell, string>(ctrl => ctrl.Subtitle,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (MaterialProfileCell)bindable;
					ctrl.Subtitle = newValue;
				});

		public static readonly BindableProperty SourceProperty = 
			BindableProperty.Create<MaterialProfileCell, ImageSource>(ctrl => ctrl.Source,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (MaterialProfileCell)bindable;
					ctrl.Source = newValue;
				});

		public ImageSource BackgroundImage {
			get{ return this.imgBackgroundImage.Source; }
			set{ this.imgBackgroundImage.Source = value; }
		}

		public Color TopBackgroundColor {
			get{ return this.gridPrincipal.BackgroundColor; }
			set{ 
				this.gridPrincipal.BackgroundColor = value; 
				this.slSuperior.BackgroundColor = value;
			}
		}

		public Color InferiorBackgroundColor {
			get{ return this.slInferior.BackgroundColor; }
			set{ this.slInferior.BackgroundColor = value; }
		}

		public string Title {
			get{ return (string)GetValue(TitleProperty); }
			set{ 
				SetValue (TitleProperty, value);
				this.lblTitle.Text = value;
			}
		}

		public string Subtitle {
			get{ return (string)GetValue(SubtitleProperty); }
			set{ 
				SetValue (SubtitleProperty, value); 
				this.lblSubtitle.Text = value;
			}
		}

		public ImageSource Source {
			get{ return this.profileImage.Source; }
			set{ this.profileImage.Source = value; }
		}

		public Color TextColor {
			get{ return this.lblTitle.TextColor; }
			set{
				this.lblTitle.TextColor = value;
				this.lblSubtitle.TextColor = value;
			}
		}

		public void ViewCellTapped(object sender, EventArgs e){

		}

		public MaterialProfileCell ()
		{
			InitializeComponent ();
		}
	}
}

