﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace CourseApp.Controls
{
	public partial class LinkImageCell : ViewCell
	{
		public Type Type {
			get;
			set;
		}

		public ImageSource ImageSource {
			get{ return this.imgItem.Source; }
			set{ this.imgItem.Source = value; }
		}

		public string Text {
			get{ return this.lblItem.Text; }
			set{ this.lblItem.Text = value; }
		}

		public LinkImageCell ()
		{
			InitializeComponent ();
		}
	}
}

