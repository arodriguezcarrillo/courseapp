﻿using System;
using Xamarin.Forms;

namespace CourseApp.Controls
{
	public class DividerCell : ViewCell
	{
		public DividerCell ()
		{
			var layout = new StackLayout () {
				Padding= new Thickness(0,8,0,8)
			};

			var divider = new BoxView () {
				Color=Color.FromRgba(0,0,0,56),
				HeightRequest=1,
				HorizontalOptions=LayoutOptions.FillAndExpand,
				VerticalOptions=LayoutOptions.Center
			};
			layout.Children.Add (divider);
			this.View = layout;
			this.Height = 17;
		}
	}
}

