﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CourseApp.Controls
{
	public partial class ImageEntry : ContentView
	{
		public static readonly BindableProperty TextProperty = 
			BindableProperty.Create<ImageEntry, string>(ctrl => ctrl.Text,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (ImageEntry)bindable;
					ctrl.Text = newValue;
				});

		public Color BorderColor {
			get{ return this.boxview.Color; }
			set{ this.boxview.Color = value; }
		}

		public ImageSource Source {
			get{ return this.image.Source; }
			set{ this.image.Source = value; }
		}

		public string Text {
			get{ return this.entry.Text; }
			set{ 
				SetValue (TextProperty, value); 
				this.entry.Text = value;
			}
		}

		public Color TextColor {
			get{ return this.entry.TextColor; }
			set{ this.entry.TextColor = value; }
		}

		public bool IsPassword {
			get{ return this.entry.IsPassword; }
			set{ this.entry.IsPassword = value; }
		}

		public Keyboard Keyboard {
			get{ return this.entry.Keyboard; }
			set{ this.entry.Keyboard = value; }
		}

		public string Placeholder {
			get{ return this.entry.Placeholder; }
			set{ this.entry.Placeholder = value; }
		}

		public ImageEntry ()
		{
			InitializeComponent ();

			if (Device.OS == TargetPlatform.Android)
				boxview.IsVisible = false;
		}
	}
}

