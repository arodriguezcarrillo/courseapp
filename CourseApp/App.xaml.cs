﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using CourseApp.Pages;

namespace CourseApp
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent ();

			AppResources.Culture =
				DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			
			MainPage = new LoginPage ();
		}

		protected override void OnSleep ()
		{
			base.OnSleep ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();
		}

		protected override void OnStart ()
		{
			base.OnStart ();
		}
	}
}

