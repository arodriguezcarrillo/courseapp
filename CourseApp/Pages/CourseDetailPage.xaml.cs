﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using CourseApp.BusinessObjects;

namespace CourseApp
{
	public partial class CourseDetailPage : ContentPage
	{
		public CourseDetailPage (Course item)
		{
			InitializeComponent ();

			this.BindingContext = item;
		}


	}
}

