﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using CourseApp.BusinessObjects;

namespace CourseApp.Pages
{
	public partial class LoginPage : ContentPage
	{
		IUserService _userService;

		public LoginPage ()
		{
			InitializeComponent ();

			this._userService = new UserServiceMockup ();
		}

		public void RegisterButtonClicked(object sender, EventArgs e){
			this.Navigation.PushModalAsync (new RegisterPage ());
		}

		public void LoginButtonClicked(object sender, EventArgs e){
			if (this._userService.Login (
				    this.emailEntry.Text,
				    this.passwordEntry.Text))
				this.Navigation.PushModalAsync (new MainPage ());
		}
	}
}

