﻿using System;
using System.Collections.Generic;
using CourseApp.BusinessObjects;
using Xamarin.Forms;

namespace CourseApp.Pages
{
	public partial class RegisterPage : ContentPage
	{
		IUserService _service;

		public RegisterPage ()
		{
			InitializeComponent ();
			this._service = new UserServiceMockup ();
		}

		public void BackButtonClicked(object sender, EventArgs e){
			this.Navigation.PopModalAsync ();
		}

		public void RegisterButtonClicked(object sender, EventArgs e){
			bool result = this._service.Register (
				this.nameEntry.Text,
				this.surnameEntry.Text,
				this.passwordEntry.Text,
				this.emailEntry.Text);

			if (result)
				this.Navigation.PopModalAsync ();
			else
				DisplayAlert(
					"Atención",
					"Se ha producido un error en el registro",
					"OK");
		}
	}
}

