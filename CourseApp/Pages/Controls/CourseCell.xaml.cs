﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CourseApp.Controls
{
	public partial class CourseCell : ViewCell
	{
		public static readonly BindableProperty ImageSourceProperty = 
			BindableProperty.Create<CourseCell, ImageSource>(ctrl => ctrl.ImageSource,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (CourseCell)bindable;
					ctrl.ImageSource = newValue;
				});

		public ImageSource ImageSource {
			get{ return (ImageSource)GetValue (ImageSourceProperty); }
			set{
				SetValue (ImageSourceProperty, value);
				this.imgsource.Source = value;
			}
		}

		public static readonly BindableProperty TitleProperty = 
			BindableProperty.Create<CourseCell, string>(ctrl => ctrl.Title,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (CourseCell)bindable;
					ctrl.Title = newValue;
				});

		public string Title {
			get{ return (string)GetValue (TitleProperty); }
			set{
				SetValue (TitleProperty, value);
				this.labelTitle.Text = value;
			}
		}

		public static readonly BindableProperty DescriptionProperty = 
			BindableProperty.Create<CourseCell, string>(ctrl => ctrl.Description,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (CourseCell)bindable;
					ctrl.Description = newValue;
				});

		public string Description {
			get{ return (string)GetValue (DescriptionProperty); }
			set{
				SetValue (DescriptionProperty, value);
				this.labelDescription.Text = value;
			}
		}

		public static readonly BindableProperty ModulesProperty = 
			BindableProperty.Create<CourseCell, int>(ctrl => ctrl.Modules,
				0,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (CourseCell)bindable;
					ctrl.Modules = newValue;
				});

		public int Modules {
			get{ return (int)GetValue (ModulesProperty); }
			set{
				SetValue (ModulesProperty, value);
				this.labelModules.Text = String.Format ("{0} módulos", value);
			}
		}
		
		public CourseCell ()
		{
			InitializeComponent ();
		}
	}
}

