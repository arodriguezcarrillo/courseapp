﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using CourseApp.BusinessObjects;

namespace CourseApp
{
	public partial class CourseListPage : ContentPage
	{
		ICourseService _service;

		public CourseListPage ()
		{
			InitializeComponent ();
			this._service = new CourseServiceMockup ();

			this.lvData.ItemsSource = this._service.Courses ();

			this.lvData.ItemTapped += (object sender, ItemTappedEventArgs e) => {
				var courseItem = (Course)e.Item;
				this.Navigation.PushAsync(new CourseDetailPage(courseItem));
			};
		}
	}
}

