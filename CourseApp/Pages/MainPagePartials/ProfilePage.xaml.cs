﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using CourseApp.BusinessObjects;

namespace CourseApp
{
	public partial class ProfilePage : ContentPage
	{
		IUserService _userService;

		public ProfilePage ()
		{
			InitializeComponent ();
			this._userService = new UserServiceMockup ();

			this.BindingContext = this._userService.GetCurrentUser ();
		}

	}
}

