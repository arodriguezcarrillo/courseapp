﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using CourseApp.BusinessObjects;

namespace CourseApp.Pages
{
	public partial class ApplicationMenu : ContentPage
	{
		IUserService _userService;
		public event EventHandler OpenProfilePage;
		public event EventHandler OpenCoursesPage;

		public ApplicationMenu ()
		{
			InitializeComponent ();
			this._userService = new UserServiceMockup ();
			this.materialProfileCell.BindingContext = this._userService.GetCurrentUser ();
		}

		public async void DisconnectTapped(object sender, EventArgs e){
			var result = await DisplayAlert(AppResources.AttentionLabel,
				AppResources.SureDisconnectLabel,
				AppResources.YesLabel, AppResources.NoLabel);

			if (result)
				await this.Navigation.PopModalAsync ();
		}

		public void ProfileTapped(object sender, EventArgs e){
			if (this.OpenProfilePage != null)
				this.OpenProfilePage (this, null);
		}

		public void CoursesTapped(object sender, EventArgs e){
			if (this.OpenCoursesPage != null)
				this.OpenCoursesPage (this, null);
		}

	}
}

