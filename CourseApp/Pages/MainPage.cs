﻿using System;

using Xamarin.Forms;
using CourseApp.BusinessObjects;

namespace CourseApp.Pages
{
	public class MainPage : MasterDetailPage
	{
		ApplicationMenu _applicationMenu;
		NavigationPage _profileNavigationPage;
		NavigationPage _coursesNavigationPage;

		public MainPage ()
		{
			this.Master = this._applicationMenu = new ApplicationMenu ();

			this._applicationMenu.OpenProfilePage += (object sender, EventArgs e) => {
				if (this._profileNavigationPage == null)
					this._profileNavigationPage = new NavigationPage (
					new ProfilePage ()
				) {
					BarBackgroundColor = Color.FromHex("00695C"),
					BarTextColor = Color.White
				};

				this.Detail = this._profileNavigationPage;
				this.IsPresented = false;
			};

			this._applicationMenu.OpenCoursesPage += (object sender, EventArgs e) => {
				this.Detail = this._coursesNavigationPage;
				this.IsPresented = false;
			};

			this.Detail = this._coursesNavigationPage = new NavigationPage (
				new CourseListPage ()
			) {
				BarBackgroundColor = Color.FromHex("00695C"),
				BarTextColor = Color.White
			};
		}
	}
}


